Biofuel is a renewable energy source, but its production is quite less despite the vast agricultural economy which produces lots of Agricultural Waste because of the defragmentation of land which makes the field generated bio waste hard to collect resulting in discontinuous supply. * Transportation cost of the Agricultural Biomass from its origin to Biofuel production plants constitutes a significant amount of the total cost associated with the biofuel production because agricultural waste is very less dense. This makes biofuel production expensive. * Lack of awareness about the value of Agricultural Waste and the benefits of Biofuel. * Air pollution due to Crop Residue burning.

IMPLEMENTATION : *A revolutionary platform for the benefits of our farmers and to increase the overall production of Biopellets with better supply chain management. * An application which will provide a single platform to the farmers and biofuel plant operators to sell agricultural waste (biomass) and to buy bio pellets. UI is available in local languages for the ease of the farmers. * Collection vehicles with an attached compressor to compress agricultural waste on-site into bio pellets to increase biomass density & reduce its transportation cost. * The in-app bidding process for biopellets buyers to ensure maximum revenue generation. Therefore, the maximum profit for the farmers. * Could be used for selling local agricultural products directly to the customers in the near future.

APPLICATION Salient Features

1) The Application have a major impact on our Environment as it will stop the agricultural residue burning in the fields and helps control air pollution.

2)The Application will contribute to the idea of Green Future where we are not dependent on our non-renewable sources of energy by increasing Biofuel production by ensuring continuous supply of bio pellets.

3)The application will have a Social and Economical impact on life of farmers as they will be earning more by selling their agricultural waste and at the same time getting awareness about the dangers to our environment and the ways to save it through the application.

4)Biomass buyers will have a Better and Consistent price and supply as there is no middle men involved in the whole process.

5)Buyers can choose from Variety of agricultural waste best suited for their biofuel production plant.

6)GPS feature enabled with path optimization enables to pick up agricultural waste efficiently and directly from the field.